#include <arpa/inet.h>
#include <curl/curl.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <magic.h>
#include <netdb.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#define READ_BLOCK_SIZE 131072  // 128 KiB

#define MAX_REQUEST_SIZE 1026
#define MAX_RESPONSE_HDR_SIZE 1028

#define CONN_BACKLOG_SIZE 128

#define STATUS_INPUT 10
#define STATUS_INPUT_SENSTV 11
#define STATUS_SUCCESS 20
#define STATUS_TEMP_REDIRECT 30
#define STATUS_PERM_REDIRECT 31
#define STATUS_TEMP_FAILURE 40
#define STATUS_SERVER_UNAVAIL 41
#define STATUS_CGI_ERR 42
#define STATUS_PROXY_ERR 43
#define STATUS_SLOW_DOWN 44
#define STATUS_PERM_FAILURE 50
#define STATUS_NOT_FOUND 51
#define STATUS_GONE 52
#define STATUS_PROXY_REFUSED 53
#define STATUS_BAD_REQUEST 59
#define STATUS_CLIENTCERT_REQ 60
#define STATUS_NOT_AUTHORIZED 61
#define STATUS_CERT_NOT_VALID 62

volatile sig_atomic_t stop = 0;

void
inthand (int signum)
{
  stop = 1;
}

struct server
{
  int sock;
  const char *host;
  SSL_CTX *ctx;
};

struct gmi_request_s
{
  char *host;
  char *path;
  int ret;  // Return code that the server will send in response
};

const char*
get_mime_type (const char *path)
{
  magic_t magic;
  const char *mime;

  char *ext = strrchr (path, '.');
  if (ext)
    if (strcmp (ext, ".gmi") == 0)
      return "text/gemini";

  magic = magic_open (MAGIC_MIME_TYPE);
  magic_load (magic, NULL);
  mime = magic_file (magic, path);

  return mime;
}

int
is_ssl_key_safe (const char *certfile, const char *keyfile,
		 const char *rootdir)
{
  char certfullpath[PATH_MAX];
  char keyfullpath[PATH_MAX];
  char rootfullpath[PATH_MAX];
  realpath (certfile, certfullpath);
  realpath (keyfile, keyfullpath);
  realpath (rootdir, rootfullpath);

  if (strncmp (certfullpath, rootfullpath, strlen (rootfullpath)) == 0)
    return 0;
  if (strncmp (keyfullpath, rootfullpath, strlen (rootfullpath)) == 0)
    return 0;
  return 1;
}

void
server_free (struct server *server)
{
  if (!server)
    return;
  if (server->ctx)
    SSL_CTX_free (server->ctx);
  if (server->sock != -1)
    close (server->sock);
  free (server);
}

void
server_create_tcp_socket (struct server *server, int port)
{
  struct sockaddr_in6 addr;
  addr.sin6_family = AF_INET6;
  addr.sin6_port = htons (port);
  addr.sin6_flowinfo = 0;
  addr.sin6_addr = in6addr_any;
  addr.sin6_scope_id = 0;

  int s = socket (AF_INET6, SOCK_STREAM, 0);
  if (s == -1)
    {
      perror ("Cannot create socket");
      server_free (server);
      exit (EXIT_FAILURE);
    }

  if (bind (s, (struct sockaddr *) &addr, sizeof (addr)) == -1)
    {
      perror ("Cannot bind to socket");
      server_free (server);
      exit (EXIT_FAILURE);
    }

  if (listen (s, CONN_BACKLOG_SIZE) == -1)
    {
      perror ("Cannot listen");
      server_free (server);
      exit (EXIT_FAILURE);
    }

  server->sock = s;
}

void
server_configure_ssl_context (struct server *server, const char *certfile,
			      const char *keyfile)
{
  if (SSL_CTX_use_certificate_file (server->ctx, certfile, SSL_FILETYPE_PEM)
      <= 0)
    {
      ERR_print_errors_fp (stderr);
      server_free (server);
      exit (EXIT_FAILURE);
    }
  if (SSL_CTX_use_PrivateKey_file (server->ctx, keyfile, SSL_FILETYPE_PEM) <= 0)
    {
      ERR_print_errors_fp (stderr);
      server_free (server);
      exit (EXIT_FAILURE);
    }
}

void
server_create_ssl_context (struct server *server)
{
  const SSL_METHOD *method;

  method = TLS_server_method ();

  server->ctx = SSL_CTX_new (method);
  if (!server->ctx) {
    perror ("Cannot create SSL context");
    ERR_print_errors_fp (stderr);
    server_free (server);
    exit (EXIT_FAILURE);
  }
}

struct server *
server_init (const char *host, const char *cert, const char *key,
	     const char *rootdir)
{
  struct server *server = malloc (sizeof (struct server));
  if (!server)
    {
      perror ("Cannot allocate memory");
      exit (EXIT_FAILURE);
    }

  server->host = host;
  server->sock = -1;
  server->ctx = NULL;

  server_create_ssl_context (server);
  server_configure_ssl_context (server, cert, key);
  server_create_tcp_socket (server, 1965);

  if (chdir (rootdir) == -1)
    {
      perror ("Cannot change to root directory");
      server_free (server);
      exit (EXIT_FAILURE);
    }

  return server;
}

/*
 * Replaces the first occurance of old char with new char
 * Returns 1 if a char was replaced, 0 if no occurance was found
 */
int str_replace_chr (char *s, char old, char new)
{
  char *hit = strchr (s, old);
  if (!hit) return 0;
  *hit = new;
  return 1;
}

struct gmi_request_s*
gmi_request (SSL *ssl)
{
  struct gmi_request_s *request = calloc (1, sizeof (struct gmi_request_s));
  if (!request)
    {
      perror ("Cannot allocate memory");
      return NULL;
    }

  char buf[MAX_REQUEST_SIZE + 1] = { 0 };
  if (SSL_read (ssl, buf, MAX_REQUEST_SIZE) == -1)
    {
      perror ("Cannot read");
      request->ret = STATUS_TEMP_FAILURE;
      return request;
    }

  int replaced = str_replace_chr (buf, '\n', '\0');
  replaced += str_replace_chr (buf, '\r', '\0');
  if (!replaced)
    {
      request->ret = STATUS_BAD_REQUEST;
      return request;
    }

  CURLU *url = curl_url ();
  if (!url)
    {
      perror ("Cannot allocate memory");
      return NULL;
    }

  if (curl_url_set (url, CURLUPART_URL, buf, CURLU_NON_SUPPORT_SCHEME) != 0)
    {
      request->ret = STATUS_BAD_REQUEST;
      curl_url_cleanup (url);
      return request;
    }

  char *host, *path;
  if (curl_url_get (url, CURLUPART_HOST, &(host), 0) != 0)
    {
      request->ret = STATUS_TEMP_FAILURE;
      curl_url_cleanup (url);
      return request;
    }
  if (curl_url_get (url, CURLUPART_PATH, &(path), 0) != 0)
    {
      request->ret = STATUS_TEMP_FAILURE;
      curl_free (host);
      curl_url_cleanup (url);
      return request;
    }

  size_t size = strlen (host) + 1;
  request->host = malloc (size);
  if (!request->host)
    {
      request->ret = STATUS_TEMP_FAILURE;
      curl_free (host);
      curl_free (path);
      curl_url_cleanup (url);
      return request;
    }
  strncpy (request->host, host, size);

  size = strlen(path) + 2;
  request->path = malloc (size);
  if (!request->path)
    {
      request->ret = STATUS_TEMP_FAILURE;
      curl_free (host);
      curl_free (path);
      curl_url_cleanup (url);
      return request;
    }
  snprintf (request->path, size, ".%s", path);

  curl_free (host);
  curl_free (path);
  curl_url_cleanup (url);
  request->ret = STATUS_SUCCESS;
  return request;
}

void
gmi_request_free (struct gmi_request_s *request)
{
  if (!request)
    return;
  if (request->host)
    free (request->host);
  if (request->path)
    free (request->path);
  free (request);
}

void
gmi_response_hdr (SSL *ssl, int ret, const char *meta, size_t meta_size)
{
  char response_hdr[MAX_RESPONSE_HDR_SIZE] = { 0 };
  size_t size = 0;
  snprintf (response_hdr, 4, "%2d ", ret);
  // Above, we specified 4 to include a null byte in snprintf
  // We don't actually want that null byte, so we only add 3 to size
  size += 3;
  strncpy (response_hdr + size, meta, meta_size);
  size += meta_size;
  strncpy (response_hdr + size, "\r\n", 2);
  size += 2;
  SSL_write (ssl, response_hdr, size);
}

void
server_response (struct server *server, SSL *ssl, struct gmi_request_s *request)
{
  if (!request)
    {
      // malloc for request failed
      gmi_response_hdr (ssl, STATUS_TEMP_FAILURE, "", 0);
      return;
    }
  if (request->ret != STATUS_SUCCESS)
    {
      gmi_response_hdr (ssl, request->ret, "", 0);
      return;
    }
  if (strcmp (server->host, request->host) != 0)
    {
      gmi_response_hdr (ssl, STATUS_PROXY_REFUSED, "", 0);
      return;
    }

  struct stat path_stat;
  stat (request->path, &path_stat);
  if (S_ISDIR (path_stat.st_mode))
    {
      size_t size = strlen(request->path) + 11;
      request->path = realloc (request->path, size);
      if (!request->path)
	{
	  gmi_response_hdr (ssl, STATUS_TEMP_FAILURE, "", 0);
	  return;
	}
      strncat (request->path, "/index.gmi", size);
    }

  const char *mime = get_mime_type (request->path);
  if (!mime)
    {
      gmi_response_hdr (ssl, STATUS_NOT_FOUND, "", 0);
      return;
    }

  int fd = open (request->path, O_RDONLY);
  if (fd == -1)
    {
      gmi_response_hdr (ssl, STATUS_NOT_FOUND, "", 0);
      return;
    }

  ssize_t size;
  char *buf = malloc (READ_BLOCK_SIZE);
  if (!buf)
    {
      perror ("Cannot allocate memory");
      gmi_response_hdr (ssl, STATUS_TEMP_FAILURE, "", 0);
      return;
    }

  gmi_response_hdr (ssl, STATUS_SUCCESS, mime, strlen(mime));
  while ((size = read (fd, buf, READ_BLOCK_SIZE)) > 0)
    SSL_write (ssl, buf, size);

  free (buf);
  close (fd);
}

void
worker (struct server *server, int client)
{
  SSL *ssl = SSL_new (server->ctx);
  SSL_set_fd (ssl, client);

  if (SSL_accept (ssl) <= 0)
    {
      ERR_print_errors_fp (stderr);
    }
  else
    {
      struct gmi_request_s *request = gmi_request (ssl);
      server_response (server, ssl, request);
      gmi_request_free (request);
    }

  SSL_shutdown (ssl);
  SSL_free (ssl);
  close (client);
  server_free (server);
  exit (EXIT_SUCCESS);
}

void
get_cli_args (int argc, char **argv, const char **host, const char **cert,
	      const char **key, const char **rootdir)
{
  const char *usagestr = "Usage: %s -c cert -k key -h host rootdir\n";
  *host = NULL;
  *cert = NULL;
  *key = NULL;
  int opt;

  while ((opt = getopt (argc, argv, "c:k:h:")) != -1)
    {
      switch (opt)
	{
	case 'c':
	  *cert = optarg;
	  break;
	case 'k':
	  *key = optarg;
	  break;
	case 'h':
	  *host = optarg;
	  break;
	default:
	  fprintf (stderr, usagestr, argv[0]);
	  exit (EXIT_FAILURE);
	}
    }

  // Check if all required arguments were passed
  if (argc - optind != 1 || !*cert || !*key || !*host)
    {
      fprintf (stderr, usagestr, argv[0]);
      exit (EXIT_FAILURE);
    }

  *rootdir = argv[optind];
}

int
main (int argc, char *argv[])
{
  const char *host, *cert, *key, *rootdir;
  get_cli_args (argc, argv, &host, &cert, &key, &rootdir);

  if (!is_ssl_key_safe (cert, key, rootdir))
    {
      fprintf (stderr, "Please store your SSL key and cert outside of your "
	       "capsule's root directory\n");
      exit (EXIT_FAILURE);
    }

  struct server *server = server_init (host, cert, key, rootdir);

  // No good way to deal with broken pipes in OpenSSL. Just ignore them
  signal (SIGPIPE, SIG_IGN);

  // We don't care about return value of our child process. Let the OS clean
  // them up
  signal (SIGCHLD, SIG_IGN);

  // Hander interrupt signal gracefully
  struct sigaction a;
  a.sa_handler = inthand;
  a.sa_flags = 0;
  sigemptyset ( &a.sa_mask);
  sigaction (SIGINT, &a, NULL);

  while (!stop)
    {
      struct sockaddr_in addr;
      unsigned int len = sizeof (addr);

      int client = accept (server->sock, (struct sockaddr *) &addr, &len);
      if (client == -1)
	{
	  perror ("Cannot accept");
	  continue;
	}

      pid_t pid = fork ();
      if (pid == 0)
	worker (server, client);
      else if (pid > 0)
	close (client);
      else
	perror ("Cannot fork");
    }

  server_free (server);
  return 0;
}
