#+TITLE: Soyuz

This is yet another minimal server for the [[https://gemini.cirumlunar.space][Gemini protocol]], written in C.  It is
currently limited to statically serving files and a bit messy (since it's all
jost one file).  I plan on turning this into a [[https://en.wikipedia.org/wiki/Literate_programming][literate program]].

* Dependencies

The following libraries are needed by Soyuz:

- libcurl
- openssl
- libmagic

* Building

The binary can be built using the following command:

#+BEGIN_EXAMPLE
  $ gcc -o soyuz soyuz.c -lcrypto -lssl -lmagic -lcurl
#+END_EXAMPLE

* Creating TLS Cert and Key

Currently, you still need to provide the TLS cert and key to the program
yourself. You can generate them using openssl from the command line as follows:

#+BEGIN_EXAMPLE
  openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -sha256 \
	  -days 365 -nodes
#+END_EXAMPLE

This generates =key.pem= and =cert.pem=, which you can then pass on in Soyuz'
command line options.
